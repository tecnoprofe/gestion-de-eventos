<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\UserController;
use App\Models\Inventory;
use App\Models\User;
use App\Models\Product;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('ejemplo',function(){
    return Inventory::all();
});

Route::get('usuarios',function(){
    return User::all();
});


Route::get('inventarios',function(){
    return Inventory::all();
});
Route::get('productos',function(){
    return Product::all();
});