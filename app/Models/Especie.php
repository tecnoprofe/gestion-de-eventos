<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Especie extends Model
{
    protected $fillable=['nomcientifico','nomvulgar','familia']; 
    use HasFactory;
    use SoftDeletes;
    protected $table = 'especie';

    // Funcion uno a muchos de ESPECIA A ANIMAL
    public function animales()
    {
        return $this->hasMany(Animal::class);
    }

    // Relacion muchos a muchos entre ZOO - ANIMAL - ESPECIE
    public function zoos()
    {
        return $this->belongsToMany(Zoo::class,'animal', 'especie_id','zoo_id' );
    }
}
