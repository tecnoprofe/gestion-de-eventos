<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Animal extends Model
{
    protected $fillable=['zoo_id','especie_id','sexo','pais']; 
    use HasFactory;
    use SoftDeletes;
    protected $table = 'animal';

    public function especie()
    {
        return $this->belongsTo(Especie::class);
    }

    
}
