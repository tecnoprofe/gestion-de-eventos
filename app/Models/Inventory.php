<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventory extends Model
{
    protected $fillable=['name','description']; 
    protected $table = 'inventory';
    use HasFactory;
    use SoftDeletes;
    public function productos(){
        return $this->belongsToMany(Product::class,'inventory_product');
    } 

}
