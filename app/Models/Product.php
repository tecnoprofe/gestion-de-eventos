<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    protected $table = 'product';
    use HasFactory;
    use SoftDeletes;

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function sales(){
        return $this->belongsToMany(Sale::class,'product_sale');
    }

    public function inventarios(){
        return $this->belongsToMany(Inventory::class,'inventory_product');
    }

}
