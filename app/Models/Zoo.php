<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Zoo extends Model
{
    protected $fillable=['nombre','ciudad','pais']; 
    use HasFactory;
    use SoftDeletes;

    protected $table = 'zoo';

    // Relacion muchos a muchos entre ZOO - ANIMAL - ESPECIE
    public function especies()
    {
        return $this->belongsToMany(Especie::class,'animal', 'zoo_id','especie_id' );
    }
}
