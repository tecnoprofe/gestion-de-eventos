<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sale extends Model
{
    protected $table = 'sale';
    use HasFactory;
    use SoftDeletes;

    public function products(){
        return $this->belongsToMany(Product::class,'product_sale');
    }
}
