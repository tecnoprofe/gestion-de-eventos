<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use OpenApi\Annotations as OA;

class UserController extends Controller
{
    /**
    * @OA\Info(
    *             title="Título que mostraremos en swagger", 
    *             version="1.0",
    *             description="Descripcion"
    * )
    *
    * @OA\Server(url="http://127.0.0.1:8000")
    */

    public function index()
    {
        return User::all();
    }
}
