<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h2>crear un nueva especie</h2>
<form method="POST" action="{{ route('animalito.store') }}">
    @csrf
    <!-- Campos del formulario para crear -->
    <label for="">Zoo</label>
    <select name="zoo_id" id="zoo_id">
        @foreach($zoologicos as $zoo)
            <option value="{{$zoo->id}}">{{$zoo->nombre}}</option>
        @endforeach       
    </select>

    <br>
    <label for="">Especie</label>
    <select name="especie_id" id="especie_id">
        @foreach($especies as $especie)
            <option value="{{$especie->id}}">{{$especie->nomcientifico}}</option>
        @endforeach       
    </select>    
    <br>
    <label for="">Sexo</label>        
        <select name="sexo" id="sexo">
            <option value="hembra">Hembra</option>
            <option value="macho">Macho</option>
        </select>
    <br>
    <label for="">Pais </label>        
    <input type="text" name="pais">
    <br>

    <button type="submit">Guardar</button>
</form>

</body>
</html>