<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  </head>
  <body>

<a href="/zoo/create">Crear </a>

<table class="table table-dark  table-striped">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Nombre</th>
      <th scope="col">Ciudad</th>
      <th scope="col">Pais</th>
      <th scope="col">Acciones</th>
    </tr>
  </thead>

  <tbody>
    @foreach ($zoos as $zoo)        
        <tr>
            <th>{{ $zoo->id }}</th>
            <td>{{ $zoo->nombre }}</td>
            <td>{{ $zoo->ciudad }}</td>
            <td>{{ $zoo->pais}}</td>
            <td>
                <a href="{{route('zoo.edit', $zoo->id)}}">Editar</a> 
                <form action="{{route('zoo.destroy', $zoo->id)}}" method="POST">
                    @csrf 
                    @method('DELETE')
                    <button type="submit">Eliminar</button>
                </form>                
            </td>
        </tr>
    @endforeach    
            
  </tbody>
</table>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
  </body>
</html>