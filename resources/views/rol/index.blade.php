<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  </head>
  <body>

<a href="/rol/create">Crear </a>

<table class="table table-dark  table-striped">
  <thead>
    <tr>
      <th scope="col">ID</th>
      <th scope="col">Nombre</th>      
      <th scope="col">Acciones</th>
    </tr>
  </thead>

  <tbody>
    @foreach ($datos as $dato)        
        <tr>
            <th>{{ $dato->id }}</th>
            <td>{{ $dato->name }}</td>            
            <td>
                <a href="{{route('rol.edit', $dato->id)}}">Editar</a> 
                <form action="{{route('rol.destroy', $dato->id)}}" method="POST">
                    @csrf 
                    @method('DELETE')
                    <button type="submit">Eliminar</button>
                </form>                
            </td>
        </tr>
    @endforeach    
            
  </tbody>
</table>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
  </body>
</html>