# Sistema de eventos

# Gestión de Eventos

Este proyecto es una aplicación para gestionar eventos desarrollada en Laravel.

## Descarga del Proyecto

Para descargar el proyecto desde el repositorio de GitLab, sigue estos pasos:

1. Abre tu navegador web y ve a la página del repositorio en GitLab: [https://gitlab.com/tecnoprofe/gestion-de-eventos/](https://gitlab.com/tecnoprofe/gestion-de-eventos/)
2. Haz clic en el botón "Download ZIP" para descargar el proyecto como un archivo ZIP.

## Instalación del Proyecto

Una vez que hayas descargado el proyecto, sigue estos pasos para instalarlo:

- Mueve la carpeta del proyecto descomprimido a la carpeta de proyectos de tu servidor local. Por ejemplo, si estás utilizando Laragon, puedes mover la carpeta a `C:/laragon/www`.
- Abre tu terminal o línea de comandos y navega hasta la carpeta del proyecto.
- Ejecuta `composer install` para instalar las dependencias del proyecto.
- Copia el archivo `.env.example` y pégalo como `.env`.
```
APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:wsmwXv6F4lNrYDKgzuvv082fWk3tkQeUGyRa4+uMXwY=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack
LOG_DEPRECATIONS_CHANNEL=null
LOG_LEVEL=debug

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=evento2
DB_USERNAME=root
DB_PASSWORD=
```
- Genera una nueva clave de aplicación con `php artisan key:generate`.
- Configura la conexión a la base de datos en el archivo `.env`.
- Ejecuta las migraciones de la BD y cargado de datos de prueba `php artisan migrate --seed`.
- npm install
- npm run dev
- Inicia el servidor local con `php artisan serve`.
- Visualiza en un navegador.