<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class General extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        //tabla category
        Schema::create('category', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();                        
            $table->string('logo')->nullable();                        
            $table->text('file')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        //tabla Product
        // fdsfsdfdsf
        //fdsfsdfsdf--- jjja jja Deiby
         
        Schema::create('product', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();            
            $table->string('stock')->nullable();            
            $table->string('brand')->nullable();            

            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('id')->on('category');

            $table->timestamps();
            $table->softDeletes();            
        });

        //tabla Sale
        Schema::create('inventory', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();                        
            $table->string('description')->nullable();                                    

            $table->timestamps();
            $table->softDeletes();
        });
        
        // ESTA TABLA COMO UNA COMPRA
        Schema::create('inventory_product', function (Blueprint $table) {            
            $table->unsignedBigInteger('product_id');
            $table->foreign('product_id')->references('id')->on('product');

            $table->unsignedBigInteger('inventory_id');
            $table->foreign('inventory_id')->references('id')->on('inventory');

            $table->primary(['product_id','inventory_id']);

            $table->timestamps();
            $table->softDeletes();
        });



        //tabla Sale
        Schema::create('sale', function (Blueprint $table) {
            $table->id();
            $table->string('price')->nullable();            
            $table->date('saledate')->nullable();

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
            $table->softDeletes();
        });

        //tabla product_sale
        Schema::create('product_sale', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('product_id');
            $table->foreign('product_id')->references('id')->on('product');

            $table->unsignedBigInteger('sale_id');
            $table->foreign('sale_id')->references('id')->on('sale');

            $table->timestamps();
            $table->softDeletes();
        });



        //tabla zoo
        Schema::create('zoo', function (Blueprint $table) {
            $table->id();
            $table->string('nombre')->nullable();
            $table->string('ciudad')->nullable();
            $table->string('pais')->nullable();
            $table->string('tamaño')->nullable();
            $table->string('presupuesto')->nullable();
            $table->timestamp('last_used_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        //tabla especie
        Schema::create('especie', function (Blueprint $table) {
            $table->id();
            $table->string('nomcientifico')->nullable();
            $table->string('nomvulgar')->nullable();
            $table->string('familia')->nullable();
            $table->string('peligro')->nullable();
            $table->timestamp('last_used_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
        // crear la tabla animal
        Schema::create('animal', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('zoo_id');
            $table->foreign('zoo_id')->references('id')->on('zoo');

            $table->unsignedBigInteger('especie_id');
            $table->foreign('especie_id')->references('id')->on('especie');


            $table->string('sexo')->nullable();
            $table->string('añonacim')->nullable();
            $table->string('pais')->nullable();
            $table->string('continente')->nullable();
            $table->timestamp('last_used_at')->nullable();
            
            $table->timestamps();
            $table->softDeletes();
        });

        

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('animal');
        Schema::dropIfExists('especie');
        Schema::dropIfExists('zoo');
        
        
        Schema::dropIfExists('product_sale');
        Schema::dropIfExists('sale');
                
        Schema::dropIfExists('inventory_product');
        Schema::dropIfExists('inventory');
        Schema::dropIfExists('product');
        Schema::dropIfExists('category');
    }
}
