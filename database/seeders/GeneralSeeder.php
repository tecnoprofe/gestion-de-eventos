<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Category;
use App\Models\Especie;
use App\Models\Product;
use App\Models\Inventory;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class GeneralSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create(['name'=>'Bebidas']);
        Category::create(['name'=>'Lacteos']);
        Category::create(['name'=>'Frutas']);
        Category::create(['name'=>'Verduras']);

        Product::create(['name'=>'Coca Cola de 2 litros','stock'=>'30','category_id'=>1,]);
        Product::create(['name'=>'Vino Rutini Arg.','stock'=>'40','category_id'=>1,]);
        Product::create(['name'=>'Agua Villa Santa 3 Ltr.','stock'=>'530','category_id'=>1,]);
        Product::create(['name'=>'Singani Casa Real Blue Label.','stock'=>'340','category_id'=>1,]);
        Product::create(['name'=>'Pepsi de 3 litros.','stock'=>'356','category_id'=>1,]);


        Inventory::create(['name'=>'Inventario 01']);
        Inventory::create(['name'=>'Inventario 02']);
        Inventory::create(['name'=>'Inventario 03']);

        $inventario01 = Inventory::find(1);
        $inventario01->productos()->attach(1);
        $inventario01->productos()->attach(2);
        $inventario01->productos()->attach(5);

        $inventario01 = Inventory::find(2);
        $inventario01->productos()->attach(3);
        $inventario01->productos()->attach(4);        
        $inventario01->productos()->attach(5);

        Role::create(['name'=>'Administrador']);
        Role::create(['name'=>'Secretaria']);
        Role::create(['name'=>'Contabilidad']);
        Role::create(['name'=>'Marketing']);
        Role::create(['name'=>'Talento Humano']);
        Role::create(['name'=>'Docente']);
        Role::create(['name'=>'Estudiante']);

        //Especie::create(['nomcientifico'=>'Ave']);
        //Especie::create(['nomcientifico'=>'Reptiles']);
        //Especie::create(['nomcientifico'=>'Mamiferos']);



    }
}

