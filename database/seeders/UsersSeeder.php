<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Faker\Factory as Faker;
class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach(range(1,100) as $i){
            User::create([
                'name'=>$faker->name,
                'paterno'=>$faker->lastName,
                'materno'=>$faker->lastName,
                'email'=>$faker->unique()->safeEmail,
                'password'=>bcrypt('1234567'),
            ]);
        }
        
        
    }
}
